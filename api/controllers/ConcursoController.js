module.exports = {
    new: (req, res) => {
        //somente admin pode criar
        //coletar se user logado é admin
        Edital.find().sort("nomeedital").exec((err, editais) => {
            return res.view({ editais: editais });
        })
    },
    resultado: (req, res) => {

        console.log(req.session.usuario.idconcurso);

        if(req.param('id')){
            req.session.usuario.idconcurso = req.param('id')
        }
        
        let sql = `select count(*), u.nome as nomeu, v.idconcurso, co.nome
        from votacao as v join public."user" as u on (u.id = v.iduser)
        join concurso as co on (co.id = v.idconcurso)
        where v.idconcurso = ${req.session.usuario.idconcurso} group by iduser, u.nome, idconcurso, co.nome`;
        dbService.get(sql, (cb) => {
            return res.view({ cb: cb.result });
        });
    },
    edit: (req, res) => {
        //somente admin pode editar somente mostrar opçao de editar para admins
        //coletar se user logado é admin
        let id = req.param('id');
        Concurso.find(id).exec((err, concurso) => {
            Edital.find().sort('nome').exec((err, editais) => {
                if (!isEdit) {
                    return res.view({ concurso: concurso, editais: editais });
                } else {
                    return res.view({ concurso: concurso, editais: null });
                }
            });
        });
    },
    index: (req, res) => {
        //listar concursos e quando clicar entrar na tela do edital
        //contar valores por um select
        /*let sql = ``
        dbService.get(sql, (cb) => {
            if (!err) {
                return res.view({ cb });
            } else {
                return res.view({ incricoes: null, concursos: null });
            }
        });*/
        Concurso.find().sort("nome").exec((err, concursos) => {
            if (!err || concursos) {
                return res.view({ concursos: concursos });// fazer chamar o edital quando clicado, passando o id do edital.
            } else {
                return res.view({ concursos: null });
            }
        });
    },
    create: (req, res) => {
        // só admin pode editar
        //coletar se user logado é admin
        let id = req.param("idconcurso");
        let concurso = {
            nome: req.param('nome'),
            descricao: req.param('descricao'),
            idedital: req.param('idedital'),
        };
        if (id) {
            Concurso.update(id, concurso, (err, conc) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar concurso, info:`, 'error');
                    return res.redirect('/concurso/edit');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/concurso');
                }
            });
        } else {
            Concurso.create(concurso, (err, concu) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao cadastrar concurso, info:`, 'error');
                    return res.redirect('/concurso/new');
                } else {
                    let result = {
                        idconcurso: concu.id,
                        numero_votos: 0,
                        idganhrador: 0,
                    }
                    Resultados.create(result, (err, resu) => {
                        flashService.createFlashPadrao(req, 1);
                        return res.redirect('/concurso');
                    });
                }
            });
        }
    },
    destroy: (req, res) => {
        Concurso.destroy(req.param('id'), (err, conc) => {
            flashService.createFlashPadrao(req, 8);
            return res.redirect('/concurso');
        });
    }
}