const Concurso = require("../models/Concurso");

module.exports = {
    new: (req, res) => {
        //somente admin pode criar
        return res.view({});
    },
    view: (req, res) => {
        //somente admin pode editar
        //coletar se user logado é admin
        let id = req.param('id');            
        Edital.find(id).exec((err, edital) => {
            if (!err) {
                return res.view({ edital: edital });
            } else {
                return res.view({ edital: null });
            }
        });
    },
    informacoes: (req, res) => {
        //mostrar editais  e poder se inscrever, será acessada da tela de concurso(index)
        Edital.find().sort("nomeedital").exec((err, editais) => {
            if (!err) {
                return res.view({ editais: editais });
            } else {
                return res.view({ editais: null });
            }
        });
    },
    create: (req, res) => {
        //somente admin pode criar
        let id = req.param("idedital");
        let edital= {
            nomeedital: req.param('nomeedital'),
            descricao: req.param('descricao'),
            dtinicio: req.param('dtinicio'),
            dtfinal: req.param('dtfinal'),
        };
        if (id) {
            Edital.update(id, edital, (err, edi) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar edital, info:`, 'error');
                    return res.redirect('/edital/edit');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/edital/informacoes');
                }
            });
        } else {
            Edital.create(edital, (err, edi) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao cadastrar edital, info:`, 'error');
                    return res.redirect('/edital/new');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/edital/informacoes');
                }
            });
        }
    },
    destroy: (req, res) => {
        
        Edital.destroy(req.param('id'), (err, edi) => {
            flashService.createFlashPadrao(req, 8);
            return res.redirect('/edital');
        });
    }
}