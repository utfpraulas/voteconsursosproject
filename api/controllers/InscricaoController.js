module.exports = {
    new: (req, res) => {
        //mostrar os concursos abertos para escolher
        //pegar o idedital achar o concurso e colocar junto  para já vir selecionado
        Concurso.find().sort('nome').exec((err, concu) => {
            return res.view({ concursos: concu });
        })
    },
    edit: (req, res) => {
        //retornar valores para poder editar, porem validar dt de inicio, se for uma data posterior nao poderá editar
        //coletar se user logado é admin
        let id = req.param('id');
        Inscricao.find(id).exec((err, inscricao) => {
            Concurso.find(inscricao.idconcurso).exec((err2, concu) => {
                if (!err && !err2) {
                    return res.view({ inscricao: inscricao, concurso: concu });
                } else {
                    return res.view({ inscricao: null, concurso: null });
                }
            })
        })
    },
    index: (req, res) => {
        //mostrar incrioes feitas

        //buscar consursos baseado no id de cada inscrição
        Inscricao.find().exec((err, inscricoes) => {
            Concurso.find().exec((err2, concu) => {
                if (!err && !err2) {
                    return res.view({ inscricoes: inscricoes, concursos: concu });
                } else {
                    return res.view({ inscricoes: null, concursos: null });
                }
            });
        })
    },
    create: (req, res) => {

        //criar inscricao
        //pegar id do edital e char o concurso o iduser será pego por ali
        let id = req.param("id");
        let iduser = req.session.usuario.id
        let inscricao = {
            idconcurso: req.param("idconcurso"),
            iduser: iduser,
            arquivo: req.param("arquivo"),
            descricao: req.param("descricao")
        };
        if (id) {
            Inscricao.update(id, inscricao, (err, insc) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar Servidor da UTFPR, info:`, 'error');
                    return res.redirect('/inscricao/edit');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/inscricao');
                }
            });
        } else {
            Inscricao.create(inscricao, (err, insc) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao cadastrar Servidor da UTFPR, info:`, 'error');
                    return res.redirect('/inscricao/new');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/inscricao');
                }
            });
        }
    },
    destroy: (req, res) => {
        Inscricao.destroy(req.param('id'), (err, insc) => {
            flashService.createFlashPadrao(req, 8);
            return res.redirect('/inscricao');
        });
    }
}