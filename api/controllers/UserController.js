const moment = require("moment");
module.exports = {
    index: (req, res) => {
        User.find().exec((err, users) => {
            if (err || !users) {
                flashService.createSessionFlash(req, `Falha ao buscar os usuários, info:  ${err}`, 'error');
                return res.view({ users: [] });
            } else {
                return res.view({ users: users });
            }
        });
    },
    update: (req, res) => {
        let id = req.param("id");
        let u = {
            nome: req.param("nome"),
            password: req.param("password"),
            email: req.param("email"),
            telefone: req.param('telefone'),
            cpf: req.param('cpf'),
            idcolabparent: 0/*req.param('idcolaparent')*/,
            dtnascimento: req.param('dtnasci'),
        }; 

        if ( req.param("ativo") == 'on'){
            u.ativo = true;
        } else {
            u.ativo = false;
        }
        if ( req.param("admin") == 'on'){
            u.is_admin = true;
        } else {
            u.is_admin = false;
        }
        if ( req.param("is_colaborador") == 'on'){
            u.is_colaborador = true;
        } else {
            u.is_colaborador = false;
        }

        if (id) {
            User.update(id, u, (err, use) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar., info:`, 'error');
                    return res.redirect('/user/edit?id='+id);
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/user');
                }
            });
        } else {
            User.create(u, (err, use) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao cadastrar usuario, info:`, 'error');
                    return res.redirect('/user/edit');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/user');
                }
            });
        }
    },
    edit: (req, res) => {
        let id = req.param('id');
        if (!id){
            return res.view({ user: null });
        } else{
            User.findOne(id, (errFO, pFO) => {
                pFO.dtnascimento = moment(pFO.dtnascimento).format('YYYY-MM-DD')
                if (errFO || !pFO) return res.view({ user: null });
                return res.view({ user: pFO });
            });
        }

    },
    destroy: (req, res) => {
        flashService.createFlashPadrao(req, 8);
        return res.redirect('/user');
    },
    login:(req,res) => {
        return res.view({ layout: '' });
    },

    logar: (req, res) => {
        if (sails.config.environment != "production") {
            let randomImage = Math.floor(Math.random() * 10 + 1);
            User.find().where({ email: 'gabriela'}).exec(function userFounded(err, user) {
                if (user && Object.keys(user).length > 0) {
                    req.session.authenticated = true;
                    req.user= user[0];
                    req.user.image = "/images/users/"+randomImage+".jpg";
                    req.user.nome = "Bem vindo!";
                    req.session.usuario = user[0];
                    req.session.usuario.image = "/images/users/"+randomImage+".jpg";
                    req.session.usuario.nome = "Bem vindo!";
                    return res.redirect('/concurso');
                }else{
                    return res.redirect('/user/login');
                }
            });
        }else{
            return res.redirect('/user/login');
        }
    },
    logout: (req, res) => {
        req.session.authenticated = false;
        req.user = null;
        req.session.usuario = null;
        return res.redirect("/user/login");
    },
    dados: (req,res) => {
        if (!req.session.usuario || !req.session.usuario.id){
            return res.redirect('/');
        } else{
            User.findOne(req.session.usuario.id, (errFO, pFO) => {
                if (errFO || !pFO) return res.redirect('/');
                return res.view({ user: pFO });
            });
        }
    },
    updateDados: (req,res) => {
        let p = {
            page: req.param('page'),
        };
        console.log("here");
        if (!p.page) {
            flashService.createSessionFlash(req, `Favor informe os dados corretamente!`, 'warning');
            return res.redirect("/user/dados");
        }
        console.log("here2");
        if (!req.session.usuario || !req.session.usuario.id) {
            console.log("here4");
            return res.redirect("/")
        }else{
            console.log("here3");
            console.log(req.session.usuario.id)
            User.update(req.session.usuario.id, p, (err, user) => {
                if (err) {
                    console.log("here5");
                    flashService.createSessionFlash(req, `Erro ao atualizar o usuário, info: ${err}`, 'error');
                }else{
                    flashService.createSessionFlash(req, `Usuário atualizado com sucesso!`, 'success');
                    req.session.usuario.page = p.page
                }
                return res.redirect('/');
            });
        }
    }
};
