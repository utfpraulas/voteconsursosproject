const moment = require("moment");

module.exports = {
    new: (req, res) => {
        Concurso.find().sort('nome').exec((err, concu) => {
            User.find().sort('nome').exec((erru, user) => {
                if(!err && !erru){
                    return res.view({concurso: concu, user: user})
                }else {
                    return res.view({concurso: null, user: null})
                }
            })
        })
    },
    create: (req, res) => {

        let vota = {
            iduser: req.param("iduser"),
            idconcurso: req.param("idconcurso"),
        };
        let dtatual = moment().format("DD/MM/YYYY HH:mm:ss");
        Concurso.find(vota.idconcurso).exec((errc, concu) => {
            let valores = concu[0].dtfinal
            valores = moment(valores).format("DD/MM/YYYY HH:mm:ss")
            if (dtatual <= valores) {
                Votacao.create(vota, (err, vot) => {
                    req.session.usuario.idconcurso = vota.idconcurso;
                    if (err) {
                        flashService.createSessionFlash(req, `Erro ao registrar votacao, info:`, 'error');
                        return res.redirect('/votacao/new');
                    } else {
                        flashService.createFlashPadrao(req, 1);
                        return res.redirect(`/concurso/resultado`);
                    }
                });
            } else {
                flashService.createSessionFlash(req, 'tempo para votação terminou.');
                        return res.redirect('/votacao/new');
            }
        });
    },
}