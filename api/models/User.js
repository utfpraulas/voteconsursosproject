const bcrypt = require('bcryptjs');

module.exports = {
  attributes: {
    //assumo que o id é autoincrement
    email:{type: 'string',required: true, unique: true},
    nome:{type: 'string', required: true},
    password:{ type: 'string', required: true },
    ativo: { type: 'boolean', defaultsTo: true },
    is_admin: { type: 'boolean', defaultsTo: false },
    telefone:{type: 'string',required: true},
    cpf:{type: 'string', required: true, unique: true},
    is_colaborador:{ type: 'boolean', required: true },
    idcolabparent: { type: 'integer', defaultsTo: 0 },
    dtnascimento: { type: 'string', required: true }
  },
  beforeCreate: function (user, cb) {
    if (user.password) {
      bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
          if (err) {
            console.log(err);
            cb(err)
          } else {
            user.password = hash;
            cb();
          }
        });
      });
    } else {
      cb();
    }
  },
  beforeUpdate: function (user, cb) {
    if (user.password) {
      bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
          if (err) {
            console.log(err);
            cb(err)
          } else {
            user.password = hash;
            cb();
          }
        });
      });
    } else {
      cb();
    }
  }
};

