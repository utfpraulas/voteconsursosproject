//TipoMSG
//info
//warning
//success
//error

module.exports = {

    createSessionFlash: function (req, parametro_msg, parametro_tipomsg) {

        let parametros = [{
            msg: parametro_msg,
            tipomsg: parametro_tipomsg
        }]

        req.session.flash = { err: parametros }
    },

    createFlashPadrao: function (req, idMSG) {
        let mensagem = "";
        let tipoMensagem = "";

        switch (idMSG) {
            case 1:
                mensagem = "Dados salvos com sucesso!";
                tipoMensagem = "success";
                break;
            case 2:
                mensagem = "Falha ao gravar os dados!";
                tipoMensagem = "error";
                break;
            case 3:
                mensagem = "Registro excluído com sucesso!";
                tipoMensagem = "success";
                break;
            case 4:
                mensagem = "Falha ao excluir o registro!";
                tipoMensagem = "error";
                break;
            case 5:
                mensagem = "Falha ao editar o registro!";
                tipoMensagem = "error";
                break;
            case 6:
                mensagem = "Dados editados com sucesso!";
                tipoMensagem = "success";
                break;
            case 7:
                mensagem = "Falha ao buscar os dados!";
                tipoMensagem = "error";
                break;
            case 8:
                mensagem = "A opção de exclusão esta desabilitada!";
                tipoMensagem = "warning";
                break;
            case 9:
                mensagem = "Informe os dados corretamente!";
                tipoMensagem = "warning";
                break;
            case 10:
                mensagem = "Já existe um registro com esses dados!";
                tipoMensagem = "warning";
                break;
        }

        let parametros = [{
            msg: mensagem,
            tipomsg: tipoMensagem
        }];

        req.session.flash = { err: parametros }
    }
};