module.exports = {
    getLabelByStatus: (status) => {
        let color = "info";

        switch (status) {
            case "in progress":
            case "running":
                color = "primary";
                break;
            case 'open':
            case 'pending':
            case 'created':
                color = "info";
                break;
            case 'closed':
            case 'success':
                color = "success";
                break;
            case 'to review':
                color = "warning";
                break;
            case 'failed':
            case 'error':
                color = "danger";
                break;
            default:
                color = "inverse";
                break;
        }
        return `<span class="label label-${color}">${status}</span>`
    }
}