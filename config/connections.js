module.exports.connections = {
  dbDev: {
    adapter: 'sails-postgresql',
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'Concursocultural',
    port:5433
  },
  local:{
    adapter: 'sails-postgresql',
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'Concursocultural',
  }
};


