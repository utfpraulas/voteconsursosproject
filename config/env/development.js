module.exports = {
  port: 80,
  models: {
    connection: 'dbDev'
  },
  db_conf: {
      adapter: 'sails-postgresql',
      host: 'localhost',
      user: 'postgres',
      password: 'postgres',
      database: 'Concursocultural',
      port:5433
  }
};
