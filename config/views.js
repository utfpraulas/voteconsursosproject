module.exports.views = {
  engine: 'ejs',
  layout: 'layouts/main',
  partials: false
};